import React from 'react';
import { StyleSheet, Text, View , Button} from 'react-native';
import {TabNavigator, createBottomTabNavigator, navigation, navigationOptions, NavigationActions} from 'react-navigation'
import Icon from 'react-native-vector-icons/Ionicons';
export  class Home extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Home</Text>
      </View>
    );
  }
}
export  class Settings extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Settings</Text>
        <Button 
      title='back to home'
      onPress={() => this.props.navigation.navigate('Home')}
      />
      </View>
    );
  }
}
class About extends React.Component{
  render(){
    return(
      <View style={styles.container}>
      <Text>Tentang Kentang</Text>
      <Button 
      title='Back to Home'
      onPress={() => this.props.navigation.navigate('Home')}
      />

      </View>
    );
    }
}

export default createBottomTabNavigator({

  Home:{screen:Home,
  navigationOptions:{
    tabBarLabel:'Home',
    tabBarIcon :({tintColor}) => (<Icon color={tintColor} name="ios-home"size={24}/>)
  }},
  Settings:{screen:Settings,
  navigationOptions:{
    tabBarLabel:'Settings',
    tabBarIcon:({tintColor}) => (<Icon  color={tintColor}name="ios-settings" size={24}/>)
  }},
  About:{
    screen:About,
    navigationOptions:{
      tabBarLabel:'About',
      tabBarIcon:({tintColor}) => (<Icon color={tintColor}name="ios-home" size={24}/>
      )
    },
  }
  
   
  
},{ //ini router config

  //halaman pertama 
  initialRouteName:'Home',

  order:['Home','Settings','About'],
  //navigation 
  navigationOptions:{
  tabBarVisible: true,

},
tabBarOptions:{
  activeTintColor:'blue',
  inactiveTintColor:'grey',
  

}
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
